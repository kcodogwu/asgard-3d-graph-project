﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;

public class phraserecognition : MonoBehaviour
{
    private string[] m_Keywords = { "show graph" };
    string word,keywords="keywords are displayed";
    
    private PhraseRecognizer m_Recognizer;
    // Start is called before the first frame update
    void Start()
    {
        GameObject textset = GameObject.Find("keyword");
        keywords = "";
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }

        m_Keywords = new string[] { "show graph" };
        for(int i = 0; i < m_Keywords.Length; i++)
        {
            keywords = keywords + m_Keywords[i];
        }
        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        
        m_Recognizer.OnPhraseRecognized += process0;
        m_Recognizer.Start();
        
        
    }

    // Update is called once per frame
    private void process0(PhraseRecognizedEventArgs args)
    {
        //StringBuilder builder = new StringBuilder();
        //builder.AppendFormat("{0} ({1}){2}", args.text, args.confidence, Environment.NewLine);
        //builder.AppendFormat("\tTimestamp: {0}{1}", args.phraseStartTime, Environment.NewLine);
        //builder.AppendFormat("\tDuration: {0} seconds{1}", args.phraseDuration.TotalSeconds, Environment.NewLine);
        //Debug.Log(builder.ToString());
        
        switch (args.text)
        {
            case "show graph":
                //Debug.Log("Said computer graph");
                //recognise1();

                GameObject pb = GameObject.Find("keyword");
                pb.GetComponent<TextMeshProUGUI>().text = "Graph is loading";
                DontDestroyOnLoad(pb);

                SceneManager.LoadSceneAsync("Assets/load graph.unity", LoadSceneMode.Additive);
               
                Practice p = new Practice();
                //p.InitializeGraph();
                
                p.DrawGraph();
                break;
        }
    }
}
