﻿using UnityEngine;
using System.Collections;
using EpForceDirectedGraph.cs;

[RequireComponent(typeof(MeshCollider))]

public class Move : MonoBehaviour
{

    private Vector3 screenPoint;
    private Vector3 offset;
    public Node node;
    public ForceDirected3D myPhysics;

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        transform.position = curPosition;
        AbstractVector v = myPhysics.GetPoint(node).position;
        v.x = curPosition.x;
        v.y = curPosition.y;
        v.z = curPosition.z;
    }

}