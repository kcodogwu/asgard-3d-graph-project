﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EpForceDirectedGraph.cs;
using TMPro;

public class DataDisplay : MonoBehaviour
{
    public Node node;

    public void Update()
    {
        node.sphere.GetComponent<TextMeshProUGUI>().text = "\n" + "ItemID: " + node.Data.itemId + "\n" + "Date: " + node.Data.itemDate;
        node.sphere.GetComponent<TextMeshProUGUI>().fontSize = 0.5f;
    }

    /*private void OnMouseOver()
    {
        node.sphere.GetComponent<TextMeshProUGUI>().text = "/n" + "ItemID: " + node.Data.itemId + "/n" + "Content: " + node.Data.itemContent + "/n" + "Date: " + node.Data.itemDate;
        node.sphere.GetComponent<TextMeshProUGUI>().fontSize = 2;
    }

    private void OnMouseExit()
    {
        node.sphere.GetComponent<TextMeshProUGUI>().text = null;
        node.sphere.GetComponent<TextMeshProUGUI>().fontSize = 2;
    }*/
}
