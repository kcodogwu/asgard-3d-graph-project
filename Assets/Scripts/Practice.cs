﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EpForceDirectedGraph.cs;
using System;
using Neo4j.Driver.V1;
using System.Linq;

public class Practice : MonoBehaviour, IDisposable
{
    public Material LineMaterial { get; private set; }
    MyRenderer myRenderer = null;
    public Graph m_fdgGraph = new Graph();
    public float stiffness = 800.00f;
    public float repulsion = 1.60f;
    public float damping   = 85.0f;

	public ForceDirected3D m_fdgPhysics;
    private IDriver _driver;
    List<IRecord> result;
    public bool show = false;

    public void Dispose()
    {
        _driver?.Dispose();
    }

    public void InitializeGraph()
    {
        using (_driver)
        {
            ConnectToGraphDB(("bolt://asgard-demo:7687"));

            using (var session = _driver.Session())
            {
                result = session.ReadTransaction(tx =>
                {
                    var r = tx.Run("MATCH (p:Message {msg_id:\"10633\"})-[r:IN_REPLY_TO]-(rp) RETURN p, type(r), rp UNION  MATCH(p:Message {msg_id:\"10633\"})-[r:HAS_USER_SENDER]-(rp) RETURN p, type(r), rp;");

                    return r.ToList();
                });


                show = true;
            }
        }
    }

    public void DrawGraph()
    {
        NodeData nd;
        NodeData nd1;
        Node node1, node2;
        EdgeData data = new EdgeData();
        Edge newEdge;
        bool isCentralNodeCreated = false;

        if (show) {

            foreach (IRecord x in result)
            {
                nd = new NodeData();
                nd1 = new NodeData();
                if (!isCentralNodeCreated)
                {
                    nd.label = "main";
                    nd.itemId = x["p"].As<INode>()["msg_id"].ToString();
                    nd.itemContent = x["p"].As<INode>()["content"].ToString();
                    nd.itemDate = x["p"].As<INode>()["date_sent"].ToString();
                    isCentralNodeCreated = true;
                    node1 = m_fdgGraph.CreateNode(nd);
                }
                else
                {
                    node1 = m_fdgGraph.GetNode("main");
                }

                nd1.itemId = x["rp"].As<INode>().Properties.ContainsKey("msg_id") ?
                    x["rp"].As<INode>()["msg_id"].ToString() :
                    x["rp"].As<INode>()["profile_id"].ToString();
                nd1.label = x["rp"].As<INode>().Properties.ContainsKey("msg_id") ? "child" : "user";
                nd1.itemContent = x["rp"].As<INode>().Properties.ContainsKey("content") ?
                    x["rp"].As<INode>()["content"].ToString() :
                    x["rp"].As<INode>()["username"].ToString();
                nd1.itemDate = x["rp"].As<INode>().Properties.ContainsKey("date_sent") ?
                    x["rp"].As<INode>()["date_sent"].ToString() :
                    x["rp"].As<INode>()["registered_date"].ToString();
                node2 = m_fdgGraph.CreateNode(nd1);
                data.label = x["type(r)"].ToString();
                data.length = 0.25f;
                newEdge = m_fdgGraph.CreateEdge(node1, node2, data);
            }

            m_fdgPhysics = new ForceDirected3D(m_fdgGraph, stiffness, repulsion, damping, 0.001f, 0f, 2.60f, 1.0f);
            m_fdgPhysics.Threadshold = 0.1f;
            m_fdgGraph.SetPhysics(m_fdgPhysics);
            myRenderer = new MyRenderer(m_fdgPhysics);
            myRenderer.Draw(Time.deltaTime);
        }
    }
    public void ConnectToGraphDB(string connUrl)
    {
        _driver = GraphDatabase.Driver(connUrl);
    }

    public void UpdateGraph(float t)
    {
        if (myRenderer != null) {
            myRenderer.Draw(t);
        }
    }
}
