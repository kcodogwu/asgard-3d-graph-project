﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RadialProgress : MonoBehaviour
{
    public GameObject LoadingText;
    public TextMeshProUGUI ProgressIndicator;
    public Image LoadingBar;
    float currentValue;
    public float speed;
    //public string LoadingSceneName;
    AsyncOperation async;

    // Use this for initialization
    void Start()
    { }

    // Update is called once per frame
    void Update()
    {
        if (currentValue < 100)
        {
            currentValue += speed * Time.deltaTime;
            GameObject.Find("ProgressIndicator").GetComponent<TextMeshProUGUI>().text = ((int)currentValue).ToString() + "%";
            LoadingText.SetActive(true);
        }
        else
        {
            LoadingText.SetActive(false);
            GameObject.Find("ProgressIndicator").GetComponent<TextMeshProUGUI>().text = "Done";
        }

        LoadingBar.fillAmount = currentValue / 100;
        //}
    }
}
