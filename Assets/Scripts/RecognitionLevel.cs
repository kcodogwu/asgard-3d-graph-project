﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;
using TMPro;
using System.Threading;

public class RecognitionLevel : MonoBehaviour
{
    [SerializeField]
    private string[] m_Keywords = { "computer" };
    private KeywordRecognizer m_Recognizer;
    public GameObject cr;
    String keywords = "keywords are displayed";
    Practice p = new Practice();
    GameObject textset;
    List<Action> listOfActions;

    void Start()
    {
        textset = GameObject.Find("keyword");
        listOfActions = new List<Action>();
        // recognise0();
        StartThreadedFunction(p.InitializeGraph);
        StartThreadedFunction(() => { RenderGraph(); });
    }

    /*
    * -- Voice menu level 0 --
    *
    * To start interaction the operator needs to say "computer"
    */

    public void recognise0()
    {
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }

        m_Keywords = new string[] { "computer" };
        keywords = "";

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            keywords = keywords + "," + m_Keywords[i];
        }

        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += process0;
        m_Recognizer.Start();

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            Debug.Log(m_Keywords[i]);
        }
    }

    private void process0(PhraseRecognizedEventArgs args)
    {
        switch (args.text)
        {
            case "computer":
                 Debug.Log("Said computer");
                 recognise1();
                 break;
                
        }
    }

    /*
    * -- Voice menu level 1 --
    *
    * "òpen"
    * "show"
    * "cancel"
    * "space"
    */

    public void recognise1()
    {
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }

        keywords = "";
        m_Keywords = new string[] { "space", "open", "show", "cancel" };

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            keywords = keywords + "," + m_Keywords[i];
        }

        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += process1;
        m_Recognizer.Start();

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            Debug.Log(m_Keywords[i]);
        }
    }

    private void process1(PhraseRecognizedEventArgs args)
    {
        switch (args.text)
        {
            case "open":
                Debug.Log("Said open");
                recognise2();
                break;
            case "space":
                Debug.Log("Said workspace");
                SceneManager.LoadScene("Assets/Room/scenes/the last revelation.unity", LoadSceneMode.Additive);
                recognise3();
                break;

            case "show":
                Debug.Log("Said show");
                recognise4();
                break;
            case "cancel":
                Debug.Log("Said cancel");
                recognise0();
                break;
        }
    }
    /*
    * -- Voice menu level 2 --
    *
    * "òpen"
    */

    public void recognise2()
    {
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }

        keywords = "";
        m_Keywords = new string[] { "users", "db", "cancel" };

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            keywords = keywords +","+ m_Keywords[i];
        }

        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += process2;
        m_Recognizer.Start();

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            Debug.Log(m_Keywords[i]);
        }
    }

    private void process2(PhraseRecognizedEventArgs args)
    {
        switch (args.text)
        {
            case "users":
                Debug.Log("Said users");
                break;
            case "db":
                Debug.Log("Said db");
                break;
            case "cancel":
                Debug.Log("Said cancel");
                recognise0();
                break;
        }
    }
    /*
    * -- Voice menu level 2 --
    *
    * "Space"
    */
    public void recognise3()
    {
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }

        keywords = "";
        m_Keywords = new string[] { "hall" };

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            keywords = keywords + "," + m_Keywords[i];
        }

        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += process3;
        m_Recognizer.Start();

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            Debug.Log(m_Keywords[i]);
        }
    }

    private void process3(PhraseRecognizedEventArgs args)
    {
        switch (args.text)
        {
            case "hall":
                Debug.Log("Said hall");
                SceneManager.UnloadSceneAsync("Assets/Urban American Assets/Demo scenes/Paint booth.unity");
                SceneManager.LoadScene("Assets/gallery_3/scenes/gallery.unity", LoadSceneMode.Additive);
                recognise0();
                break;
            case "cancel":
                Debug.Log("Said cancel");
                recognise0();
                break;

        }
    }
    /*
    * -- Voice menu level 2 --
    *
    * "Show"
    */
    public void recognise4()
    {
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }

        keywords = "";
        m_Keywords = new string[] { "graph", "cancel" };

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            keywords = keywords +","+ m_Keywords[i];
        }

        textset.GetComponent<TextMeshProUGUI>().text = keywords;
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += process4;
        m_Recognizer.Start();

        for (int i = 0; i < m_Keywords.Length; i++)
        {
            Debug.Log(m_Keywords[i]);
        }
    }

    private void process4(PhraseRecognizedEventArgs args)
    {
        switch (args.text)
        {
            case "graph":
                Debug.Log("Said graph");
                cr.SetActive(true);
                StartThreadedFunction(p.InitializeGraph); // child thread to query
                StartThreadedFunction(() => { RenderGraph(); }); // list of functions executed on the main thread
                break;
            case "cancel":
                Debug.Log("Said cancel");
                recognise0();
                break;
        }
    }

    private void OnApplicationQuit()
    {
        if (m_Recognizer != null)
        {
            m_Recognizer.Stop();
            m_Recognizer.Dispose();
        }
    }

    void StartThreadedFunction(Action func) {
        Thread t = new Thread(new ThreadStart(func));

        t.Start();
    }

    void RenderGraph() {
        Action f = () => { p.DrawGraph(); };

        QueueThreadedFunctions(f);
    }

    public void QueueThreadedFunctions(Action f) {
        listOfActions.Add(f);
    }

    void Update() {
        if (p.show) {
            cr.SetActive(false);
            Action fx = listOfActions[0];

            listOfActions.RemoveAt(0);
            fx();
            p.show = false;
            Debug.Log("Loaded");
        }

        //p.UpdateGraph(Time.deltaTime);
    }
}
