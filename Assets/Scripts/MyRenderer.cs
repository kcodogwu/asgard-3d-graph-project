using EpForceDirectedGraph.cs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class MyRenderer: AbstractRenderer
{
    public MyRenderer(IForceDirected iForceDirected) : base(iForceDirected)
    {
        // Your initialization to draw
    }

    public override void Clear()
    {
        // Clear previous drawing if needed
        // will be called when AbstractRenderer:Draw is called
    }
   
    protected override void drawEdge(Edge iEdge, AbstractVector iPosition1, AbstractVector iPosition2)
    {
        // Draw the given edge according to given positions
        Vector3[] p = { new Vector3(iPosition1.x, iPosition1.y, iPosition1.z), new Vector3(iPosition2.x, iPosition2.y, iPosition2.z) };
        iEdge.line.SetPositions(p);
    }

    protected override void drawNode(Node iNode, AbstractVector iPosition)
    {
        // Draw the given node according to given position
        Vector3 position = new Vector3(iPosition.x, iPosition.y, iPosition.z);
        iNode.sphere.transform.SetPositionAndRotation(position,Quaternion.identity);
        if ( iNode.Data.label == "main")
        {
            iNode.sphere.GetComponent<Renderer>().material.SetColor("_Color",Color.red);
        }
        else if (iNode.Data.label == "child")
        {
            iNode.sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
        }
        else if (iNode.Data.label == "user")
        {
            iNode.sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
        }
    }
};